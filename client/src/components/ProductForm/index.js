import React, { useEffect, useContext } from 'react';
import { Formik, Form, Field } from 'formik';
import { Button, LinearProgress } from '@material-ui/core';
import * as Yup from 'yup'
import { TextField } from 'formik-material-ui';
import { createFetchProduct, getFetchProduct, updateFetchProduct } from '../../utils/API';
import { Context } from '../../redux/store';

export const FormWithFormik = ({ _id }) => {
  const { state, dispatch } = useContext(Context)
  const isEdit = !!_id


  let initialValues = {
    title: '',
    description: '',
    imageUrl: '',
    price: '',
  }

  useEffect(() => {
    if (isEdit) {
      getFetchProduct(dispatch, _id)
    }
  }, [_id])

  const createProduct = (values) => {
    return createFetchProduct(dispatch, values)
      .then(() => {
        return Promise.resolve()
      })
      .catch(err => err)
  }

  const updateProduct = (values) => {
    return updateFetchProduct(dispatch, values)
      .then(() => {
        getFetchProduct(dispatch, _id)
        return Promise.resolve()
      })
      .catch(err => err)
  }

  const fetch = isEdit ? updateProduct : createProduct
  const btnTitle = isEdit ? 'Edit product' : 'Add product'

  if (isEdit) {
    if (state.product) {
      return (
        <MyFormik
          initialValues={state.product}
          handleSubmit={fetch}
          btnTitle={btnTitle}
        />
      )
    }
    return null
  }

  return (
    <MyFormik
      initialValues={initialValues}
      handleSubmit={fetch}
      btnTitle={btnTitle}
    />
  )
}

function MyFormik({initialValues, handleSubmit, btnTitle}) {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={Yup.object({
        title: Yup.string()
          .min(3, 'Must be at least 3 characters')
          .max(30, 'Must be 30 characters or less')
          .required('Required'),
        description: Yup.string()
          .min(15, 'Must be at least 15 characters')
          .required('Required'),
        imageUrl: Yup.string()
          .required('Required'),
        price: Yup.number()
          .min(1, 'Must be at least 1')
          .max(999999, 'Must be 999999 or less')
          .required('Required')
      })}
      onSubmit={(values, { setSubmitting, resetForm, setValues }) => {
        setSubmitting(true)
        handleSubmit(values)
          .then(() => {
            setSubmitting(false)
            resetForm()
          })
        setValues(values)
        
      }}
    >
      {({ isSubmitting, resetForm }) => (
        <Form
          className='Form-product'
        >
          <Field
            component={TextField}
            name="title"
            label="Title"
            placeholder='Title your product'
          />
          <Field
            component={TextField}
            name="imageUrl"
            label="Image URL"
            placeholder="Link your product's image"
          />
          <Field
            component={TextField}
            multiline={true}
            name="description"
            label="Descriptoin"
            placeholder="Description your product"
          />
          <Field
            component={TextField}
            name="price"
            label="Price"
            placeholder="Price your produt"
          />
          {isSubmitting && <LinearProgress />}
          <div className='Form-product__btn-wrap'>
            <Button
              variant='outlined'
              color='primary'
              disabled={isSubmitting}
              type='submit'
            >
              {btnTitle}
            </Button>
            <Button
              variant='outlined'
              color='secondary'
              onClick={resetForm}
            >
              Resset Fields
          </Button>
          </div>
        </Form>
      )}
    </Formik>
  )
}