import React from 'react'
import { useParams } from 'react-router-dom'
import { Layout } from '../../components/Layout'
import { FormWithFormik } from '../../components/ProductForm/'
import '../../scss/Form-product.scss'

export const FormProductPage = () => {
  const { _id } = useParams()
  const titleNavbar = _id ? 'Edit Product' : 'Add Product'
  return (
    <Layout titleNavbar={titleNavbar}>
      <h1>{_id ? 'Edit Page' : 'Add Product Page'}</h1>
      <FormWithFormik _id={_id}/>
    </Layout>
  )
}
